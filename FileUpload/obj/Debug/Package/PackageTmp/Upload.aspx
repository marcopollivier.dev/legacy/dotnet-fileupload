﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="FileUpload._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<script language='javascript' type='text/javascript'>window.parent.CallBackFunction('<%=fileName  %>');</script>
</body>
</html>


<script runat="server">
    
    protected string fileName="";

    protected void Page_Load(object sender, EventArgs e)
    {
        HttpPostedFile myFile = Request.Files["arquivo"] as HttpPostedFile;
        String sessionId = Request.Form["SID"];

        if (sessionId == null)
            sessionId = "";
        else
            sessionId = sessionId.Substring(1, 8);
        
        if (myFile != null && myFile.ContentLength != 0)
        {
            fileName = sessionId + myFile.FileName;
            
            myFile.SaveAs(@"C:\Temp\" + fileName);
        }
        else
        {
            fileName = "";
        }
       
    }

</script>